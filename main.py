from abc import ABC, abstractmethod
import random

class Animal(ABC):
  instances_list = []

  def __init__(self, name, weight):
    self.name = name
    self.weight = weight
    Animal.instances_list.append(self)

  def feed(self, food):
    print('{} получил {}'.format(self.name, food))
    self.speak()

  @abstractmethod
  def harvest(self):
    self.speak()

  @abstractmethod
  def speak(self):
    pass

  def get_name(self):
    return '{} ({})'.format(self.name, type(self).__name__)


  def __eq__(self, other):
    return self.weight == other.weight

  def __lt__(self, other):
    return self.weight < other.weight

  def __le__(self, other):
    return self.weight <= other.weight


  def __int__(self):
    return int(self.weight)

  def __float__(self):
    return float(self.weight)

  def __add__(self, other):
    return float(self) + float(other)

  def __radd__(self, other):
    return self.__add__(other)


class Bird(Animal):
  def harvest(self):
    print('Собрали яйца у {}'.format(self.name))
    super().harvest()


class Goose(Bird):
  def speak(self):
    print('{}: Го-го'.format(self.get_name()))


class Chicken(Bird):
  def speak(self):
    print('{}: Ко-ко'.format(self.get_name()))


class Duck(Bird):
  def speak(self):
    print('{}: Кря-кря'.format(self.get_name()))


class Mammals(Animal):
  def harvest(self):
    print('Доим {}'.format(self.name))
    super().harvest()


class Cow(Mammals):
  def speak(self):
    print('{}: Му-у-у'.format(self.get_name()))


class Goat(Mammals):
  def speak(self):
    print('{}: Ме-е-е'.format(self.get_name()))


class Sheep(Mammals):
  def harvest(self):
    print('Подстригаем {}'.format(self.name))
    # super().harvest()
    self.speak()

  def speak(self):
    print('{}: Бе-е-е'.format(self.get_name()))


print('Ферма Дядюшки Джо:\n')

goose1 = Goose('Серый', 6.5)
goose2 = Goose('Белый', 8)
# goose1.feed('зерно')
# goose2.feed('топинамбур')

cow = Cow('Манька', 507)
# cow.feed('сено')

sheep1 = Sheep('Барашек', 83)
sheep2 = Sheep('Кудрявый', 94)
# sheep1.feed('зеленую травку')
# sheep2.feed('силос')

hen = Chicken('Ко-ко', 4)
cock = Chicken('Кукареку', 4.4)
# hen.feed('гречку')
# cock.feed('овес')

goat1 = Goat('Рога', 46)
goat2 = Goat('Копыта', 49)
# goat1.feed('овощную ботву')
# goat2.feed('овощи')

duck = Duck('Кряква', 3.2)
# duck.feed('чечевицу')

# farm_uncle_joe = [goose1, goose2, cow, sheep1, sheep2, hen, cock, goat1, goat2, duck]

farm_uncle_joe = Animal.instances_list

food_list = [
  'зерно', 'топинамбур', 'сено', 'зелеая травка', 'силос', 'гречка', 'овес', 'овощная ботва', 'овощи', 'чечевица'
]

for animal in farm_uncle_joe:
  animal.feed(random.choice(food_list))
  animal.harvest()
  print()

print('Общий вес всех животных на ферме: {}'.format(sum(farm_uncle_joe)))

heaviest = max(farm_uncle_joe)
print('Самый тяжелый на ферме: {}'. format(heaviest.get_name()))
